//
//  ViewController.swift
//  Assignment3
//
//  Created by Aecio Levy on 2018-07-09.
//  Copyright © 2018 Aecio Levy. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    @IBOutlet weak var viewController: UICollectionView!
    let emojis = [["👨🏼", "👨🏼‍💻", "🤦🏼‍♂️", "🌝"], ["🌙", "🌍", "🔥", "🌈" ] ]
    override func viewDidLoad() {
        super.viewDidLoad()
        viewController.dataSource = self
        viewController.delegate = self
        viewController.register(UINib(nibName: "CollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "customCell")
        // Do any additional setup after loading the view, typically from a nib.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }


}

extension ViewController : UICollectionViewDelegate, UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: 100, height: 100)
    }
    
}

extension ViewController : UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return emojis[0].count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "customCell", for: indexPath) as! CollectionViewCell
        let emoji = emojis[indexPath.section][indexPath.row]
        cell.emojiLabel.text = emoji
        return cell
    }
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return emojis.count
    }
    
    
}
